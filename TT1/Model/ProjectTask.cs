﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace TT1
{
    public class ProjectTask 
    {
        public int TaskId { get; set; }          
        public string Description { get; set; }
        public string Remarks { get; set; }
        public int ProjectId { get; set; }
        public string Priority { get; set; }
        public enum Priorities { LO, ME, HI }
    }
}
