﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;



namespace TT1
{
    public class Project
    {

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; }
        public string CreationDate { get; set; }
        public string ModificationDate { get; set; }
        public ObservableCollection<Task> TaskList { get; set; }


        public static async Task<ObservableCollection<Project>> GetProjects()
        {
            ObservableCollection<Project> projectList = new ObservableCollection<Project>();

            var client = new HttpClient();
            
            client.BaseAddress = new Uri("http://10.30.134.9:60606/");
     //       client.BaseAddress = new Uri("http://192.168.120.219:60606/");
            
            var response = await client.GetAsync("api/Projects");

            if (response.IsSuccessStatusCode)
            {
                //parse the body
                //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                var result = await response.Content.ReadAsStringAsync();
                var projects = JsonConvert.DeserializeObject<ObservableCollection<Project>>(result);

                projectList = projects;
            }
            return projectList;
        }


        public async Task<ObservableCollection<TT1.ProjectTask>> GetTasksFromProject(int projectId)
        {
            ObservableCollection<TT1.ProjectTask> taskList = new ObservableCollection<ProjectTask>();

            var client = new HttpClient();
                       client.BaseAddress = new Uri("http://10.30.134.9:60606/");
           // client.BaseAddress = new Uri("http://192.168.120.219:60606/");
            string api = "api/Tasks/" + projectId.ToString();
            var response = await client.GetAsync(api);
            

            if (response.IsSuccessStatusCode)
            {
                //parse the body
                //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                var result = await response.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<ObservableCollection<TT1.ProjectTask>>(result);

                taskList = tasks;
            }
            return taskList;
        }


  

 
    }
}
