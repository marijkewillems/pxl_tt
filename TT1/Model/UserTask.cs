﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Model
{
    public class UserTask
    {
        public int TaskId { get; set; }  
        public int UserId { get; set; }
        public string CreationDate { get; set; }
        public string Comments { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
