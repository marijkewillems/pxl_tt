﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1
{
    public class Utils
    {
        public static Project LoadedProject { get; set; }
        public static string SerializeObject(object obj)
        {
            MemoryStream stream = new MemoryStream();
            System.Runtime.Serialization.Json.DataContractJsonSerializer ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType(), App1.Common.SuspensionManager.KnownTypes);
            ser.WriteObject(stream, obj);
            byte[] data = new byte[stream.Length];
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(data, 0, data.Length);
            return Convert.ToBase64String(data);
        }

        public static Project DeserializeObject<Project>(string input)
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(Project));
            MemoryStream stream = new MemoryStream(Convert.FromBase64String(input));
            return (Project)ser.ReadObject(stream);
        }
    }
}
