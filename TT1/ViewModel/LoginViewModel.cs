﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT1.Model;
using TT1.Repository;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TT1.ViewModel
{
    public class LoginViewModel:ViewModelBase
    {

        private RelayCommand loginCommand;
        public RelayCommand LoginCommand
        {
            get { return loginCommand; }
            set { loginCommand = value; }
        }


        private RelayCommand cancelCommand;
        public RelayCommand CancelCommand
        {
            get { return cancelCommand; }
            set { cancelCommand = value; }
        }

        public LoginViewModel()
        {
            loginCommand = new RelayCommand(OnLogin);
            cancelCommand = new RelayCommand(OnCancel);
        }

        private User _currentUser;
        public User CurrentUser
        {
            get { return _currentUser; }
            set { SetProperty<User>(ref _currentUser, value); }
        }

        private string _currentUserName;
        public string CurrentUserName
        {
            get { return _currentUserName; }
            set { SetProperty<string>(ref _currentUserName, value); }
        }

        private string _currentPassword;
        public string CurrentPassword
        {
            get { return _currentPassword; }
            set { SetProperty<string>(ref _currentPassword, value); }
        }

        private string _errorMelding;
        public string ErrorMelding
        {
            get { return _errorMelding; }
            set { SetProperty<string>(ref _errorMelding, value); }
        }

 
        private async void OnLogin()
        {
            IUsersRepository _usersRepository = new UsersRepository(); ;
            
            CurrentUser = await _usersRepository.GetUserByNameAsync(CurrentUserName);
            if(CurrentUser != null)
            {
                if(CurrentUser.Password == CurrentPassword)
                {
                    ErrorMelding = "";
                    var content = Window.Current.Content;
                    var frame = content as Frame;

                    if (frame != null)
                    {
                        TimeTrackerSingleton instance = TimeTrackerSingleton.Instance();
                        instance.CurrentUser = CurrentUser;
                        frame.Navigate(typeof(ProjectView),CurrentUser.UserId);
                    }
                }
                else
                {
                    ErrorMelding = "Wrong Password!";
                }
            }
            else
            {
                ErrorMelding = "User doesn't exist";
            }
          
 
        }

        private void OnCancel()
        {
            
        }
    }
}
