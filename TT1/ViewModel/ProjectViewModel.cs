﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using App1.Common;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml;
using TT1.Repository;




namespace TT1.ViewModel
{
    public class ProjectViewModel : ViewModelBase
    {
        private ObservableCollection<Project> _projects;
        private RelayCommand projectSelectCommand;
        private IQueueRepository _queueRepository;


        public ProjectViewModel()
        {
            projectSelectCommand = new RelayCommand(OnProjectSelected);
            TimeTrackerSingleton instance = TimeTrackerSingleton.Instance();
            _queueRepository = new QueueRepository();
        }

        async private void LoadProjects(int userId)
        {
            IProjectsRepository _projectRepository= new ProjectsRepository();;
            TimeTrackerSingleton instance = TimeTrackerSingleton.Instance();
            instance.Projects = await _projectRepository.GetProjectsByUserAsync(userId);
            Projects = instance.Projects;
           
        }

        private void OnProjectSelected()
        {

            var content = Window.Current.Content;
            var frame = content as Frame;

            if (frame != null)
            {
                frame.Navigate(typeof(ProjectTaskView), Utils.SerializeObject(SelectedProject) );
            }
        }

        public RelayCommand ProjectSelectCommand
        {
            get { return projectSelectCommand; }
            set { projectSelectCommand = value; }
        }
        
  
        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                SetProperty<Project>(ref _selectedProject, value);
            }
        }

        private int _currentUserId;
        public int CurrentUserId
        {
            get { return _currentUserId; }
            set
            {
                SetProperty<int>(ref _currentUserId, value);
            }
        }


        public ObservableCollection<Project> Projects
        {

            get { return _projects; }
            set
            {
                SetProperty<ObservableCollection<Project>>(ref _projects, value);
 
            }
        }

        private void OnAddProject()
        {
            _queueRepository.AddProject(SelectedProject);
        }

        private async void OnSaveQueue()
        {
            await _queueRepository.SaveQueue();            
        }

        public override async void LoadState(object sender, LoadStateEventArgs e)
        {
            IProjectsRepository _projectRepository = new ProjectsRepository(); ;
            TimeTrackerSingleton instance = TimeTrackerSingleton.Instance();
  //          CurrentUser.UserId = Convert.ToInt32(e.NavigationParameter);
            instance.Projects = await _projectRepository.GetProjectsByUserAsync(Convert.ToInt32(e.NavigationParameter));
            CurrentUserId = Convert.ToInt32(e.NavigationParameter);
            Projects = instance.Projects;
        }


    }
}
