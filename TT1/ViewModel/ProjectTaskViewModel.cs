﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT1.Repository;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace TT1.ViewModel
{
    public class ProjectTaskViewModel:ViewModelBase
    {
        private RelayCommand projectTaskSelectCommand;

        public ProjectTaskViewModel()
        {
            projectTaskSelectCommand = new RelayCommand(OnProjectTaskSelected);
        }

        public RelayCommand ProjectTaskSelectCommand
        {
            get { return projectTaskSelectCommand; }
            set { projectTaskSelectCommand = value; }
        }


        private void OnProjectTaskSelected()
        {

            var content = Window.Current.Content;
            var frame = content as Frame;

            if (frame != null)
            {
                frame.Navigate(typeof(UserTaskView), Utils.SerializeObject(SelectedProjectTask));
            }
        }

        private Project _currentProject;
        public Project CurrentProject
        {
            get { return _currentProject; }
            set { SetProperty<Project>(ref _currentProject, value); }

        }
        private ObservableCollection<ProjectTask> _projectTasks;
        public ObservableCollection<ProjectTask> ProjectTasks
        {
            get { return _projectTasks; }
            set { SetProperty<ObservableCollection<ProjectTask>>(ref _projectTasks, value); }
        }

        private ProjectTask _selectedProjectTask;
        public ProjectTask SelectedProjectTask
        {
            get { return _selectedProjectTask; }
            set { SetProperty<ProjectTask>(ref _selectedProjectTask, value); }
        }

        async private Task LoadProjectTasks(Project project)
        {
            IProjectTasksRepository _projectTasksRepository = new ProjectTasksRepository(); ;
            ProjectTasks = await _projectTasksRepository.GetProjectTasksFromProjectAsync(project.ProjectId);
        }

        public async override void LoadState(object sender, LoadStateEventArgs e)
        {
            CurrentProject = Utils.DeserializeObject<Project>((string)e.NavigationParameter);
            await LoadProjectTasks(CurrentProject);
        }

        public override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e != null)
            {
                CurrentProject = Utils.DeserializeObject<Project>((string)e.Parameter);
            }
        }
    }
}
