﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.ViewModel
{
    public class TimeTrackerSingleton
    {
        private  ObservableCollection<Project> _projects;
        private static TimeTrackerSingleton _instance;
        private  User _currentUser;
        private ProjectTask _currentProjectTask;

        private TimeTrackerSingleton()
        {}
        public static TimeTrackerSingleton Instance()
        {
            if(_instance==null)
                _instance = new TimeTrackerSingleton();
            return _instance;
        }
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set
            {
                if (value != _projects)
                {
                    _projects = value;                    
                }
            }
        }
        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                if (value != _currentUser)
                {
                    _currentUser = value;
                }
            }
        }

        public ProjectTask CurrentProjectTask
        {
            get { return _currentProjectTask; }
            set
            {
                if (value != _currentProjectTask)
                {
                    _currentProjectTask = value;
                }
            }
        }
    }
}
