﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;

namespace TT1.ViewModel
{
    public class ViewModelBase: BindableBase
    {  
        public virtual void LoadState(object sender, LoadStateEventArgs e) { }
        public virtual void SaveState(object sender, SaveStateEventArgs e) { }

        public virtual void OnNavigatedTo(NavigationEventArgs e) { }

       public INavigationService NavigationService {get;set;}
        public virtual bool CanGoBack
        {
            get
            {
                if (NavigationService != null) return NavigationService.CanGoBack;
                else return false;
            }
        }
        public virtual bool CanGoForward
        {
            get
            {
                if (NavigationService != null) return NavigationService.CanGoForward;
                else return false;
            }
        }

    }
}
