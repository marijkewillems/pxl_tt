﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using TT1.Repository;
using App1.Common;
using Windows.UI.Xaml.Navigation;

namespace TT1.ViewModel
{
    public class TaskViewModel : ViewModelBase
    {
        private Project _currentProject;
        public Project CurrentProject
        {
            get { return _currentProject; }
            set { SetProperty<Project>(ref _currentProject, value); }
            
        }
        private ObservableCollection<ProjectTask> _tasks;
        public ObservableCollection<ProjectTask> Tasks
        {
            get { return _tasks; }
            set { SetProperty < ObservableCollection<ProjectTask>>(ref _tasks, value); }
        }

        private ProjectTask _selectedTask;
        public ProjectTask SelectedTask
        {
            get { return _selectedTask; }
            set { SetProperty<ProjectTask>(ref _selectedTask, value); }
        }

        async private Task LoadProjectTasks(Project project)
        {
            IProjectTasksRepository _taskRepository = new ProjectTasksRepository(); ;
            Tasks = await _taskRepository.GetProjectTasksFromProjectAsync(project.ProjectId);        
        }

        public override void LoadState(object sender, LoadStateEventArgs e)
        {
            CurrentProject = Utils.DeserializeObject<Project>((string)e.NavigationParameter);  
        }

        public override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e != null)
            {                
                CurrentProject = Utils.DeserializeObject<Project>((string)e.Parameter);                
            }
        }
    }
}
