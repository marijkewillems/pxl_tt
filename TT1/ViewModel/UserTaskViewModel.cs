﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT1.Model;
using TT1.Repository;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace TT1.ViewModel
{
    public class UserTaskViewModel:ViewModelBase
    {
        private bool contin, stop, save, newUserTask;
        public bool Contin
        {
            get { return contin; }
            set { contin = value; }
        }
        public bool Stop
        {
            get { return stop; }
            set { stop = value; }
        }
        public bool Save
        {
            get { return save; }
            set { save = value; }
        }
        public bool NewUserTask
        {
            get { return newUserTask; }
            set { newUserTask = value; }
        }


        private RelayCommand openCommandBarCommand;

        public RelayCommand OpenCommandBarCommand
        {
            get { return openCommandBarCommand; }
            set { openCommandBarCommand = value; }
        }

        private RelayCommand userTaskSelectCommand;

        public RelayCommand UserTaskSelectCommand
        {
            get { return userTaskSelectCommand; }
            set { userTaskSelectCommand = value; }
        }

        private RelayCommand newUserTaskCommand;

        public RelayCommand NewUserTaskCommand
        {
            get { return newUserTaskCommand; }
            set { newUserTaskCommand = value; }
        }
        private RelayCommand continueUserTaskCommand;

        public RelayCommand ContinueUserTaskCommand
        {
            get { return continueUserTaskCommand; }
            set { continueUserTaskCommand = value; }
        }

        private RelayCommand stopUserTaskCommand;

        public RelayCommand StopUserTaskCommand
        {
            get { return stopUserTaskCommand; }
            set { stopUserTaskCommand = value; }
        }

        private RelayCommand saveUserTaskCommand;

        public RelayCommand SaveUserTaskCommand
        {
            get { return saveUserTaskCommand; }
            set { saveUserTaskCommand = value; }
        }

        public UserTaskViewModel()
        {
            userTaskSelectCommand = new RelayCommand(OnUserTaskSelected);
            newUserTaskCommand = new RelayCommand(OnNewUserTask);
            continueUserTaskCommand = new RelayCommand(OnContinueUserTask);
            stopUserTaskCommand = new RelayCommand(OnStopUserTask);
            saveUserTaskCommand = new RelayCommand(OnSaveUserTask);
            openCommandBarCommand = new RelayCommand(OnOpenCommandBar);
        }

        private void OnNewUserTask()
        {
            newUserTask = true;
 //           SelectedUserTask = new UserTask();
 //           SelectedUserTask.TaskId = CurrentProjectTask.TaskId;
 //           SelectedUserTask.UserId = CurrentUser.UserId;
            var content = Window.Current.Content;
            var frame = content as Frame;

            if (frame != null)
            {
                frame.Navigate(typeof(AddUserTaskView));
            }
        }

        private void OnOpenCommandBar()
        {
            TestButtons();
        }


        private void OnUserTaskSelected()
        {
            newUserTask = false;
            var content = Window.Current.Content;
            var frame = content as Frame;

            if (frame != null && SelectedUserTask!=null)
            {
                frame.Navigate(typeof(AddUserTaskView), Utils.SerializeObject(SelectedUserTask));
           
            }
        }

        private void OnContinueUserTask()
        {
            contin = true;
            stop = false;
            save = false;
            if(SelectedUserTask!=null)
            {              
                SelectedUserTask.StartTime = DateTime.Now.ToString();
                SelectedUserTask.EndTime = null;
                OnPropertyChanged("SelectedUserTask");
            }
            TestButtons();
        }

        private bool continueButtonEnabled;
        public bool ContinueButtonEnabled
        {
            get { return continueButtonEnabled; }
            set
            {
                continueButtonEnabled = value;
                ContinueUserTaskCommand.CanExecute(value);
                OnPropertyChanged("ContinueButtonEnabled");
            }
            
        }

        private bool stopButtonEnabled;
        public bool StopButtonEnabled
        {
            get { return stopButtonEnabled; }
            set {   stopButtonEnabled = value;
                    stopUserTaskCommand.CanExecute(value);
                    OnPropertyChanged("StopButtonEnabled");
            }
        }

        private bool saveButtonEnabled;
        public bool SaveButtonEnabled
        {
            get { return saveButtonEnabled; }
            set
            {
                saveButtonEnabled = value;
                SaveUserTaskCommand.CanExecute(value);
                OnPropertyChanged("SaveButtonEnabled");
            }

        }

        private  void OnStopUserTask()
        {
            stop = true;
            contin = false;
            save = false;

            SelectedUserTask.EndTime = DateTime.Now.ToString();
           // if(!newUserTask)
                OnPropertyChanged("SelectedUserTask");

            TestButtons();
           
        }

        private async void OnSaveUserTask()
        {
            save = true;
            contin=false;
            stop=false;
                await AddUserTask(SelectedUserTask);
                TestButtons();

            var content = Window.Current.Content;
            var frame = content as Frame;

            if (frame != null)
            {
                frame.GoBack();
            }
        }

        private UserTask _selectedUserTask;
        public UserTask SelectedUserTask
        {
            get { return _selectedUserTask; }
            set
            {
                SetProperty<UserTask>(ref _selectedUserTask, value);
            }
        }


        public void TestButtons()
        {
    
            if (newUserTask)
            {
                if (stop)
                {
                    StopButtonEnabled = false;
                    ContinueButtonEnabled = false;
                    if ((SelectedUserTask.Comments != String.Empty && SelectedUserTask.Comments != null) &&
                        (SelectedUserTask.CreationDate != String.Empty && SelectedUserTask.CreationDate != null) &&
                        (SelectedUserTask.EndTime != String.Empty && SelectedUserTask.EndTime != null) &&
                        (SelectedUserTask.StartTime != String.Empty && SelectedUserTask.StartTime != null))
                        SaveButtonEnabled = true;
                    else
                        SaveButtonEnabled = false;

                }
                else
                {
                    ContinueButtonEnabled = false;
                    StopButtonEnabled = true;
                    SaveButtonEnabled = false;

                    
                }
            }
            else
            {
                if (contin)
                {
                    StopButtonEnabled = true;
                    ContinueButtonEnabled = false;
                    SaveButtonEnabled = false;
                }
                else
                { 
                    if(stop)
                    {
                        StopButtonEnabled = false;
                        ContinueButtonEnabled = false;
                        if ((SelectedUserTask.Comments != String.Empty && SelectedUserTask.Comments!=null) && 
                            (SelectedUserTask.CreationDate != String.Empty && SelectedUserTask.CreationDate != null) &&
                            (SelectedUserTask.EndTime != String.Empty && SelectedUserTask.EndTime != null) && 
                            (SelectedUserTask.StartTime != String.Empty && SelectedUserTask.StartTime != null))
                            SaveButtonEnabled = true;
                        else
                            SaveButtonEnabled = false;
                    
                    }
                    else // UserTask geselecteerd maar nog op geen enkele button geklikt
                    {
                        ContinueButtonEnabled = true;
                        SaveButtonEnabled = false;
                        StopButtonEnabled = false;
                    }
                }
                

            }
        }

        private ObservableCollection<UserTask> userTasks;
        public ObservableCollection<UserTask> UserTasks
        {         

            get { return userTasks; }
            set
            {
                if (value != userTasks)
                {
                    userTasks = value;
                    OnPropertyChanged("UserTasks");
                }
            }
        }

        private ProjectTask currentProjectTask;
        public ProjectTask CurrentProjectTask
        {
            get { return currentProjectTask; }
            set
            {
                SetProperty<ProjectTask>(ref currentProjectTask, value);
            }
        }

        private User currentUser;
        public User CurrentUser
        {
            get { return currentUser; }
            set
            {
                SetProperty<User>(ref currentUser, value);
            }
        }

        private async Task LoadUserTasks(ProjectTask projectTask, User user)
        {
            IUserTasksRepository _userTasksRepository = new UserTasksRepository(); ;
            UserTasks = await _userTasksRepository.GetUserTasksFromProjectTaskAsync(user, projectTask);
        }

        private async Task AddUserTask(UserTask userTask)
        {
            IUserTasksRepository _userTasksRepository = new UserTasksRepository(); ;
            int count = await _userTasksRepository.AddUserTaskAsync(userTask);
        }

        private async Task UpdateUserTask(UserTask userTask)
        {
            IUserTasksRepository _userTasksRepository = new UserTasksRepository(); ;
            int count = await _userTasksRepository.UpdateUserTaskAsync(userTask);
        }

        public override async void LoadState(object sender, LoadStateEventArgs e)
        {
            if (e.NavigationParameter != null)
            {
            CurrentProjectTask = Utils.DeserializeObject<ProjectTask>((string)e.NavigationParameter);
            TimeTrackerSingleton instance = TimeTrackerSingleton.Instance();
            CurrentUser = instance.CurrentUser;
            instance.CurrentProjectTask = CurrentProjectTask;
            
            await LoadUserTasks(CurrentProjectTask, CurrentUser);
            //      await LoadUserTasks(CurrentProject,CurrentUser);

                /*
                Windows.Storage.ApplicationDataContainer roamingSettings =
               Windows.Storage.ApplicationData.Current.RoamingSettings;
                 if (roamingSettings.Values.ContainsKey("contin"))
                {
                    contin = (bool)roamingSettings.Values["contin"];
                }

                 if (roamingSettings.Values.ContainsKey("save"))
                 {
                     save = (bool)roamingSettings.Values["save"];
                 }
                 if (roamingSettings.Values.ContainsKey("stop"))
                 {
                     stop = (bool)roamingSettings.Values["stop"];
                 }
                 if (roamingSettings.Values.ContainsKey("newUserTaks"))
                 {
                     newUserTask = (bool)roamingSettings.Values["newUserTask"];
                 }*/
             }
    
        }

        public override void SaveState(object sender, SaveStateEventArgs e)
        {

            base.SaveState(sender, e);
            Windows.Storage.ApplicationDataContainer roamingSettings =
               Windows.Storage.ApplicationData.Current.RoamingSettings;
            
            /*
            roamingSettings.Values["contin"] = contin;
            roamingSettings.Values["stop"] = stop;
            roamingSettings.Values["save"] = save;
            roamingSettings.Values["newUserTask"] = newUserTask;
 //           roamingSettings.Values["selectedUserTask"] = SelectedUserTask;
             * */

        }
    }
}
