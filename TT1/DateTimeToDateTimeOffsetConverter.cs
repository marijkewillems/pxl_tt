﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace TT1
{
    public class DateTimeToDateTimeOffsetConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                
                DateTime date = DateTime.ParseExact(value.ToString(), "d/M/yyyy hh:mm:ss", provider);
                return new DateTimeOffset(date);
            }
            catch (Exception ex)
            {
                return DateTimeOffset.MinValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            try
            {
                DateTimeOffset dto = (DateTimeOffset)value;
                return dto.DateTime.ToString("d/M/yyyy hh:mm:ss");
            }
            catch (Exception ex)
            {
                return DateTime.MinValue.ToString("d/M/yyyy hh:mm:ss");
            }
        }
    }
}
