﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT1.Model;

namespace TT1.Repository
{
    public interface IUserTasksRepository
    {
        Task<ObservableCollection<UserTask>> GetUserTasksFromProjectTaskAsync(User user, ProjectTask projectTask);
        Task<int> AddUserTaskAsync(UserTask userTask);
        Task<int> UpdateUserTaskAsync(UserTask userTask);
    }


}
