﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Repository
{
    public class ProjectTasksRepository:IProjectTasksRepository
    {
        public async Task<ObservableCollection<TT1.ProjectTask>> GetProjectTasksFromProjectAsync(int projectId)
        {
            ObservableCollection<TT1.ProjectTask> taskList = new ObservableCollection<ProjectTask>();

            var client = new HttpClient();
            client.BaseAddress = UriFactory.Instance().BaseUri;
            string api = "api/Tasks/" + projectId.ToString();
            var response = await client.GetAsync(api);


            if (response.IsSuccessStatusCode)
            {
                //parse the body
                //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                var result = await response.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<ObservableCollection<TT1.ProjectTask>>(result);

                taskList = tasks;
            }
            return taskList;
        }
    }
}
