﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Repository
{
    public interface IProjectsRepository
    {
        Task<ObservableCollection<Project>> GetProjectsAsync();
        Task<ObservableCollection<TT1.Project>> GetProjectsByUserAsync(int userId);
    }
}
