﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT1.Model;

namespace TT1.Repository
{
    public interface IUsersRepository
    {
        Task<TT1.User> GetUserByNameAsync(string userName);
    }
}
