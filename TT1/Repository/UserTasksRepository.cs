﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TT1.Model;

namespace TT1.Repository
{

        public class UserTasksRepository : IUserTasksRepository
        {
            public async Task<ObservableCollection<UserTask>> GetUserTasksFromProjectTaskAsync(User user, ProjectTask projectTask)
            {
                ObservableCollection<UserTask> userTaskList = new ObservableCollection<UserTask>();

                var client = new HttpClient();
                client.BaseAddress = UriFactory.Instance().BaseUri;
                string api = "api/UserTasks/" + user.UserId + "/" + projectTask.TaskId;
                var response = await client.GetAsync(api);


                if (response.IsSuccessStatusCode)
                {
                    //parse the body
                    //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                    var result = await response.Content.ReadAsStringAsync();
                    var userTasks = JsonConvert.DeserializeObject<ObservableCollection<UserTask>>(result);

                    userTaskList = userTasks;
                }
                return userTaskList;
            }

            public async Task<int> AddUserTaskAsync(UserTask userTask)
            {
                var client = new HttpClient();
                client.BaseAddress = UriFactory.Instance().BaseUri;

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string api = "api/UserTasks/";


                Uri uri = new Uri(client.BaseAddress + api);
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uri);
                
                string json = JsonConvert.SerializeObject(userTask);
                httpRequestMessage.Content = new StringContent(json,Encoding.UTF8,"application/json");
              
                var response = await client.PostAsync(uri, httpRequestMessage.Content);
                if (response.IsSuccessStatusCode)
                {
                    // Get the URI of the created resource.
                    Uri gizmoUrl = response.Headers.Location;
                }
             
                return 1;
            }
        


            public async Task<int> UpdateUserTaskAsync(UserTask userTask)
            {
                var client = new HttpClient();
                client.BaseAddress = UriFactory.Instance().BaseUri;

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string api = "api/UserTasks/";


                Uri uri = new Uri(client.BaseAddress + api);
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, uri);
                
                string json = JsonConvert.SerializeObject(userTask);
                httpRequestMessage.Content = new StringContent(json,Encoding.UTF8,"application/json");
              
                var response = await client.PutAsync(uri, httpRequestMessage.Content);
                if (response.IsSuccessStatusCode)
                {
                    // Get the URI of the created resource.
                    Uri gizmoUrl = response.Headers.Location;
                }
             
                return 1;
            }
        }
    }

