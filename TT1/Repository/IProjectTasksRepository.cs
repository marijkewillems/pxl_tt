﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Repository
{
    public interface IProjectTasksRepository
    {
        Task<ObservableCollection<TT1.ProjectTask>> GetProjectTasksFromProjectAsync(int projectId);
    }
}


