﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Data.Services.Client;
using Newtonsoft.Json;

namespace TT1.Repository
{
    public class ProjectsRepository: IProjectsRepository
    {
        public async Task<ObservableCollection<Project>> GetProjectsAsync()
        {
            ObservableCollection<Project> projectList = new ObservableCollection<Project>();

            var client = new HttpClient();

            client.BaseAddress = UriFactory.Instance().BaseUri;

            var response = await client.GetAsync("Projects");

            if (response.IsSuccessStatusCode)
            {
                //parse the body
                //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                var result = await response.Content.ReadAsStringAsync();
                var projects = JsonConvert.DeserializeObject<ObservableCollection<Project>>(result);

                projectList = projects;
            }

            return projectList;

        }

        public async Task<ObservableCollection<TT1.Project>> GetProjectsByUserAsync(int userId)
        {
            ObservableCollection<TT1.Project> projectList = new ObservableCollection<Project>();

            var client = new HttpClient();
            client.BaseAddress = UriFactory.Instance().BaseUri;
            string api = "api/Projects/" + userId.ToString();
            var response = await client.GetAsync(api);


            if (response.IsSuccessStatusCode)
            {
                //parse the body
                //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                var result = await response.Content.ReadAsStringAsync();
                var projects = JsonConvert.DeserializeObject<ObservableCollection<TT1.Project>>(result);

                projectList = projects;
            }
            return projectList;
        }
    }
}
