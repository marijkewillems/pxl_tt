﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TT1.Model;

namespace TT1.Repository
{

        public class UsersRepository : IUsersRepository
        {
            public async Task<TT1.User> GetUserByNameAsync(string userName)
            {
                TT1.User u = new User();

                var client = new HttpClient();
                client.BaseAddress = UriFactory.Instance().BaseUri;
             
                userName = userName.Replace(".", "-");
                string api = "api/Users/name/" + userName;
                var response = await client.GetAsync(api);


                if (response.IsSuccessStatusCode)
                {
                    //parse the body
                    //var categories = response.Content.ReadAsAsync<IEnumerable<Category>>().Result; // blocks
                    var result = await response.Content.ReadAsStringAsync();
                    var user = JsonConvert.DeserializeObject<TT1.User>(result);

                    u = user;
                }
                return u;
            }
        
    }
}
