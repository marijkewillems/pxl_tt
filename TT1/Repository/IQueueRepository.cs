﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Repository
{
    public interface IQueueRepository
    {
        void AddProject(Project project); // Add Projects to the queue
        ObservableCollection<Project> AllProjectsInQueue(); // Return all items in the queue
        Task SaveQueue();
    }
}
