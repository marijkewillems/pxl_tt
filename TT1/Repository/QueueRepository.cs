﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Repository
{
    public class QueueRepository: IQueueRepository
    {
        ObservableCollection<Project> _Projects = new ObservableCollection<Project>();
        public void AddProject(Project project)
        {
            _Projects.Add(project);
        }

        public ObservableCollection<Project> AllProjectsInQueue()
        {
            return _Projects;
        }

        public Task SaveQueue()
        {
             return Task.Factory.StartNew( () =>
            {
               Task.Delay(3000);
            });
        }
    }
}
