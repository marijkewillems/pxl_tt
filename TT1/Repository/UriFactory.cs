﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT1.Repository
{
    class UriFactory
    {
        private static UriFactory instance;

       //private Uri baseUri = new Uri("http://localhost:60606/");
        private Uri baseUri = new Uri("http://pxltt.azurewebsites.net/");
       // private Uri baseUri = new Uri("http://192.168.120.219:60606/");
        public static UriFactory Instance()
        {
            if (instance == null)
            { 
                instance = new UriFactory();
            }
            return instance;
        }

        public Uri BaseUri
        {
            get { return baseUri; }
            private set {}
        }
    }
}
