﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TT1.Model;
using TT1.ViewModel;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace TT1
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
   //         this.DebugSettings.IsBindingTracingEnabled = true;
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        /// 
 //       async protected override void OnLaunched(LaunchActivatedEventArgs e)
         protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = false;
            }
#endif
            

            // Retrieve splash screen object.
            //SplashScreen splashScreen = e.SplashScreen;

            //ExtendedSplash eSplash = new ExtendedSplash(splashScreen);

            //Window.Current.Content = eSplash;
            //Window.Current.Activate();

  //          TimeTrackerSingleton instance = TimeTrackerSingleton.Instance();
  //          instance.Projects = await Project.GetProjects();
  

            // Tear down the extended splash screen after all operations are complete.
            //RemoveExtendedSplash();

            Frame rootFrame = Window.Current.Content as Frame;



            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                SuspensionManager.RegisterFrame(rootFrame, "appFrame");
                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application

                    UserTask ut = new UserTask();
                    SuspensionManager.KnownTypes.Add(ut.GetType());
                    await SuspensionManager.RestoreAsync();
                }

                if(e.PreviousExecutionState == ApplicationExecutionState.ClosedByUser || e.PreviousExecutionState == ApplicationExecutionState.NotRunning)
                {
                    Windows.Storage.ApplicationDataContainer roamingSettings =
                Windows.Storage.ApplicationData.Current.RoamingSettings;
                    roamingSettings.Values.Remove("creationDate");
                    roamingSettings.Values.Remove("remarks");
                    roamingSettings.Values.Remove("startTime");
                    roamingSettings.Values.Remove("endTime");

                    roamingSettings.Values.Remove("stop");
                    roamingSettings.Values.Remove("save");
                    roamingSettings.Values.Remove("contin");
                    roamingSettings.Values.Remove("newUserTask");

                    roamingSettings.Values.Remove("user");
                    roamingSettings.Values.Remove("projectTask");
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }
            
            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
  
                
                rootFrame.Navigate(typeof(LoginView), e.Arguments);
            }
            // Ensure the current window is active
            Window.Current.Activate();


        }

        void RemoveExtendedSplash()
        {
            Window.Current.Content = new ProjectView();
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            /*
            Frame currentFrame = Window.Current.Content as Frame;
            AddUserTaskView addUserTaskPage = currentFrame.Content as AddUserTaskView;

            AddUserTaskViewModel vm = (AddUserTaskViewModel)addUserTaskPage.DataContext;

            
            SuspensionManager.SessionState["addUserTaskState"] = vm.SelectedUserTask;
            SuspensionManager.KnownTypes.Add(vm.SelectedUserTask.GetType());
       */
            await SuspensionManager.SaveAsync();

            deferral.Complete();
        }
    }
}
