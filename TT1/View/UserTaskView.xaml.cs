﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TT1.View;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Group Detail Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234229

namespace TT1
{
    /// <summary>
    /// A page that displays an overview of a single group, including a preview of the items
    /// within the group.
    /// </summary>
    public sealed partial class UserTaskView : ViewBase
    {
        public UserTaskView()
        {
            this.InitializeComponent();
 
        }

        protected override void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            base.navigationHelper_LoadState(sender, e);
            Windows.Storage.ApplicationDataContainer roamingSettings =
                Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values.Remove("creationDate");
            roamingSettings.Values.Remove("remarks");
            roamingSettings.Values.Remove("startTime");
            roamingSettings.Values.Remove("endTime");

            roamingSettings.Values.Remove("stop");
            roamingSettings.Values.Remove("save");
            roamingSettings.Values.Remove("contin");
            roamingSettings.Values.Remove("newUserTask");

            roamingSettings.Values.Remove("user");
            roamingSettings.Values.Remove("projectTask");
        }
    }
}
