﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TT1.View;
using TT1.ViewModel;
using System.Globalization;
using TT1.Model;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace TT1
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class AddUserTaskView : ViewBase
    {
        public AddUserTaskView()
        {
            this.InitializeComponent();

        }

        private void RemarksTextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings =
               Windows.Storage.ApplicationData.Current.RoamingSettings;

            roamingSettings.Values["remarks"] = RemarksTextBox.Text;
        }


        private void UserTaskDate_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {

            Windows.Storage.ApplicationDataContainer roamingSettings =
               Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["creationDate"] = UserTaskDate.Date;            
        }

        private void StartTimeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings =
   Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["startTime"] = StartTimeTextBox.Text; 
        }

        private void EndTimeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Windows.Storage.ApplicationDataContainer roamingSettings =
  Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["endTime"] = EndTimeTextBox.Text; 
        }

        protected override void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {

            base.navigationHelper_LoadState(sender, e);
            // Restore values stored in app data.
            Windows.Storage.ApplicationDataContainer roamingSettings =
                Windows.Storage.ApplicationData.Current.RoamingSettings;
            
            AddUserTaskViewModel vm = this.DataContext as AddUserTaskViewModel;
            
            if (vm == null)
            {
                AddUserTaskViewModel addUserTaskModel = new AddUserTaskViewModel();
                if (SuspensionManager.SessionState.ContainsKey("addUserTaskState"))
                {
                    vm = SuspensionManager.SessionState["addUserTaskState"] as AddUserTaskViewModel;

                    if (vm != null)
                    {
                        this.DataContext = vm;
                    }
                }
            }         
            
   
            
                if (roamingSettings.Values.ContainsKey("creationDate"))
                {
                    UserTaskDate.Date = (DateTimeOffset)roamingSettings.Values["creationDate"];               
                    vm.SelectedUserTask.CreationDate = UserTaskDate.Date.ToString("d/M/yyyy hh:mm:ss");

                }

                if (roamingSettings.Values.ContainsKey("remarks"))
                {
                    RemarksTextBox.Text = roamingSettings.Values["remarks"].ToString();
                    vm.SelectedUserTask.Comments = RemarksTextBox.Text;
                }

                if (roamingSettings.Values.ContainsKey("startTime"))
                {
                    StartTimeTextBox.Text = roamingSettings.Values["startTime"].ToString();
                    vm.SelectedUserTask.StartTime = StartTimeTextBox.Text;
                }

                if (roamingSettings.Values.ContainsKey("endTime"))
                {
                    EndTimeTextBox.Text = roamingSettings.Values["endTime"].ToString();
                    vm.SelectedUserTask.EndTime = EndTimeTextBox.Text;
                }

                
                
                 if (roamingSettings.Values.ContainsKey("contin"))
                {
                    vm.Contin = (bool)roamingSettings.Values["contin"];
                }

                 if (roamingSettings.Values.ContainsKey("save"))
                 {
                     vm.Save = (bool)roamingSettings.Values["save"];
                 }
                 if (roamingSettings.Values.ContainsKey("stop"))
                 {
                     vm.Stop = (bool)roamingSettings.Values["stop"];
                 }
                 if (roamingSettings.Values.ContainsKey("newUserTaks"))
                 {
                     vm.NewUserTask = (bool)roamingSettings.Values["newUserTask"];
                 }

                 
        }

      

        private void CommBar1_Opened(object sender, object e)
        {
            

            AddUserTaskViewModel vm = this.DataContext as AddUserTaskViewModel;
           
            vm.TestButtons();
        }

        protected override void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            base.navigationHelper_SaveState(sender, e);
 
       
        }

        private void ProjectTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

   

    }
}
