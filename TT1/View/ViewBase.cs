﻿using App1.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using TT1.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Navigation;

namespace TT1.View
{
    public class ViewBase:Page
    {


        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ViewBase()
        {
            BindingOperations.SetBinding(this, DataContextChangedWatcherProperty, new Binding());
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        protected virtual void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            var vm = DataContext as ViewModelBase;
            if (vm != null) vm.LoadState(sender, e);
        }

        protected  virtual void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            var vm = DataContext as ViewModelBase;
            if (vm != null) vm.SaveState(sender, e);
        }
        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

      
        public static readonly DependencyProperty DataContextChangedWatcherProperty =
            DependencyProperty.Register("DataContextChangedWatcherProperty", typeof(object), typeof(ViewBase), new PropertyMetadata(null, OnDataContextChanged));

        public object DataContextChangedWatcher
        {
            get { return (object)GetValue(DataContextChangedWatcherProperty); }
            set { SetValue(DataContextChangedWatcherProperty, value); }
        }

        private static void OnDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var vm = ((ViewBase)d).DataContext as ViewModelBase;
            if (vm != null) vm.NavigationService = NavigationService.Current;
        }
    }
}
