﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.UI.Xaml;
using Microsoft.Xaml.Interactions.Core;
using Microsoft.Xaml.Interactions.Media;
using Microsoft.Xaml.Interactivity;
using Windows.UI.Xaml.Controls;

namespace TT1
{
    public class CallMethodOnLoadBehavior : Behavior<FrameworkElement>
    {

        public string MethodName
        {
            get { return (string)base.GetValue(MethodNameProperty); }
            set { SetValue(MethodNameProperty, value); }
        }

        public static readonly DependencyProperty MethodNameProperty = 
            DependencyProperty.Register("MethodName", typeof(string), typeof(CallMethodOnLoadBehavior), new PropertyMetadata(null));

        public object TargetObject
        {
            get { return (object)GetValue(TargetObjectProperty); }
            set { SetValue(TargetObjectProperty, value); }
        }

        public static readonly DependencyProperty TargetObjectProperty =
            DependencyProperty.Register("TargetObject", typeof(object), typeof(CallMethodOnLoadBehavior), new PropertyMetadata(null));

        public object TasksProject
        {
            get { return (object)base.GetValue(TasksProjectProperty); }
            set { SetValue(TasksProjectProperty, value); }
        }

        public static readonly DependencyProperty TasksProjectProperty =
            DependencyProperty.Register("TasksProject", typeof(object), typeof(CallMethodOnLoadBehavior), new PropertyMetadata(null));


        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Loaded += OnLoad;
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            if (AssociatedObject == null || string.IsNullOrWhiteSpace(MethodName) || TargetObject == null || DesignMode.DesignModeEnabled) return;
            MethodInfo minfo = TargetObject.GetType().GetTypeInfo().GetDeclaredMethod(MethodName);
            if (minfo == null) return;

            if (TasksProject == null)
                minfo.Invoke(TargetObject, null);
            else
            {
                minfo.Invoke(TargetObject, new object[] { TasksProject });
            }
        }

 
    }
}
