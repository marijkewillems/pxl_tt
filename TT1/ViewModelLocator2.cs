﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;

namespace TT1.ViewModel
{
    public class ViewModelLocator2
    {
        UserTaskViewModel utVM;
        public BindableBase ProjectView
        {
            get
            {
                return new ProjectViewModel();
            }
        }

        public BindableBase ProjectTaskView
        {
            get
            {
                return new ProjectTaskViewModel();
            }        
        }

        //nog Interface toevoegen --> geen code-behind
        public BindableBase LoginView
        {
            get
            {
                return new LoginViewModel();
            }
        }
        public BindableBase UserTaskView
        {
            get
            {
                return new UserTaskViewModel();
            }
        }
        public BindableBase AddUserTaskView
        {
            get
            {
                return new AddUserTaskViewModel();
            }
        }
    }
}
